﻿using Emgu.CV.CvEnum;
using Emgu.CV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using Rectangle = System.Drawing.Rectangle;
using System.Text.RegularExpressions;
using System.Windows.Media.Media3D;
using System.Windows;
using System.Security.Policy;
using System.Reflection.Metadata;
using Point = System.Windows.Point;

namespace Yang
{   
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : System.Windows.Window
    {
        private Game _game;
        public MainWindow()
        { 
            InitializeComponent();
            _game = new Game();
            _game.OnLog += _game_OnLog;
            _game.OnHistoryChange += _game_OnHistoryChange;
        }


        private void _game_OnHistoryChange(List<Block> obj)
        {
            this.Dispatcher.Invoke(() =>
            {
                Tree_Data.Items.Clear();
                foreach (Block b in obj)
                {
                    if (b.Visiable == true && b.VisiableLevel == 0)
                    {
                        Tree_Data.Items.Add(b);
                    }
                }
                ExpandAllItems(Tree_Data);
            });
        }
        private  void ExpandAllItems(ItemsControl control)
        {
            if (control == null)
            {
                return;
            }

            foreach (Object item in control.Items)
            {
                System.Windows.Controls.TreeViewItem? treeItem = control.ItemContainerGenerator.ContainerFromItem(item) as TreeViewItem;


                if (treeItem == null || !treeItem.HasItems)
                {
                    continue;
                }

                treeItem.IsExpanded = true;

                ExpandAllItems(treeItem);
            }
        }
        private void _game_OnLog(string obj)
        {
            this.Dispatcher.Invoke(() => {
                TxtLog.AppendText(obj);
                TxtLog.AppendText(Environment.NewLine);
                TxtLog.ScrollToEnd();
                this.Activate();
            });
        }


        private void ButtonStart_Click(object sender, RoutedEventArgs e)
        {
            int step = 6;
            int.TryParse(Txt_MaxStep.Text, out step);
            Task task = _game.StartAsync(CheckAuto.IsChecked, step);
        }

        private void ButtonStop_Click(object sender, RoutedEventArgs e)
        {
            _game.Stop();
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            _game.ClearHistory();
        }

        private void ButtonTest_Click(object sender, RoutedEventArgs e)
        {
            _game.Test();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_game._screen != null)
            {
                _game._screen.Dispose();
            }
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            _game.Stop();
            var result = MessageBox.Show("保存数据将覆盖之前保存的数据，确定么", "确认", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                _game.Save();
            }
        }

        private void ButtonLoad_Click(object sender, RoutedEventArgs e)
        {
            _game.Load();
            MessageBox.Show("读取完成");
        }


        private void ButtonTestPath_Click(object sender, RoutedEventArgs e)
        {
            int step = 6;
            int.TryParse(Txt_MaxStep.Text, out step);
            Task task = _game.TestPathAsync(step);
        }

    }
}
