﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yang
{
    public class BlockGroup
    {
        public List<Block> Blocks { get; set; }
        public int ValidClick { get; set; }

        public List<Block> Current { get; set; }
        public BlockGroup(IEnumerable<Block> bs)
        {
            this.Blocks = bs.ToList();
        }

        public bool Contains(int id)
        {
            if (Blocks == null)
            {
                return false;
            }
            else
            {
                return Blocks.Any(x => x.ID == id);
            }
        }
        public bool Contains(Block b)
        {
            if (b == null)
            {
                return false;
            }
            else
            {
                return this.Contains(b.ID);
            }
        }

    }
}
