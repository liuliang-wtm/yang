﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Yang
{
    public enum ENxButtonActionTypes
    {
        None,
        MouseDown,
        MouseMove,
        MouseUp,
    }
    public class CNxHookEventArgs : EventArgs
    {
        //public CNxHookEventArgs(ENxButtonActionTypes in_ActionType, Point in_Location, int in_ClickCount, MouseButtons in_MouseButtons)
        //{
        //    m_ButtonActionType = in_ActionType;
        //    m_Location = in_Location;
        //    m_ClickCount = in_ClickCount;
        //    m_MouseButton = in_MouseButtons;
        //}
        public void Initialize(ENxButtonActionTypes in_ActionType, Point in_Location, int in_ClickCount, MouseButton in_MouseButtons)
        {
            m_ButtonActionType = in_ActionType;
            m_Location = in_Location;
            m_ClickCount = in_ClickCount;
            m_MouseButton = in_MouseButtons;
        }

        private ENxButtonActionTypes m_ButtonActionType;
        public ENxButtonActionTypes ButtonActionType
        {
            get { return m_ButtonActionType; }
        }
        private Point m_Location;
        public Point Location
        {
            get { return m_Location; }
        }

        private int m_ClickCount;
        public int ClickCount
        {
            get { return m_ClickCount; }
        }
        private MouseButton m_MouseButton;
        public MouseButton MouseButton
        {
            get { return m_MouseButton; }
        }
    }


    public class CNxMouseHook
    {
        private const int WM_MOUSEMOVE = 0x200;
        private const int WM_LBUTTONDOWN = 0x201;
        private const int WM_RBUTTONDOWN = 0x204;
        private const int WM_MBUTTONDOWN = 0x207;
        private const int WM_LBUTTONUP = 0x202;
        private const int WM_RBUTTONUP = 0x205;
        private const int WM_MBUTTONUP = 0x208;
        private const int WM_LBUTTONDBLCLK = 0x203;
        private const int WM_RBUTTONDBLCLK = 0x206;
        private const int WM_MBUTTONDBLCLK = 0x209;
        //全局的事件
        public event EventHandler<CNxHookEventArgs> MouseEvent;
        static int m_hMouseHook = 0;   //鼠标钩子句柄
        //鼠标常量
        public const int WH_MOUSE_LL = 14;   //mouse   hook   constant
        HookProc m_MouseHookProcedure;   //声明鼠标钩子事件类型.
        [StructLayout(LayoutKind.Sequential)]
        public class CNxPOINT
        {
            public int x;
            public int y;
        }
        //声明鼠标钩子的封送结构类型
        [StructLayout(LayoutKind.Sequential)]
        public class CNxMouseHookStruct
        {
            public CNxPOINT pt;
            public int hWnd;
            public int wHitTestCode;
            public int dwExtraInfo;
        }
        //装置钩子的函数
        [DllImport("user32.dll ", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern int SetWindowsHookEx(int idHook, HookProc lpfn, IntPtr hInstance, int threadId);
        //卸下钩子的函数
        [DllImport("user32.dll ", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool UnhookWindowsHookEx(int idHook);

        //下一个钩挂的函数
        [DllImport("user32.dll ", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern int CallNextHookEx(int idHook, int nCode, Int32 wParam, IntPtr lParam);
        public delegate int HookProc(int nCode, Int32 wParam, IntPtr lParam);
        [DllImport("kernel32.dll")]
        private static extern IntPtr GetModuleHandle(IntPtr path);

        private CNxHookEventArgs m_MouseArgs;

        ///   <summary>
        ///   墨认的构造函数构造当前类的实例.
        ///   </summary>
        public CNxMouseHook()
        {
            m_MouseArgs = new CNxHookEventArgs();
        }
        //析构函数.
        ~CNxMouseHook()
        {
            Stop();
        }
        public void Start()
        {
            //安装鼠标钩子
            if (m_hMouseHook == 0)
            {
                //生成一个HookProc的实例.
                m_MouseHookProcedure = new HookProc(MouseHookProc);
                m_hMouseHook = SetWindowsHookEx(WH_MOUSE_LL, m_MouseHookProcedure, GetModuleHandle(IntPtr.Zero), 0);
                //如果装置失败停止钩子
                if (m_hMouseHook == 0)
                {
                    Stop();
                    throw new Exception("SetWindowsHookEx failed. ");
                }
            }
        }
        public void Stop()
        {
            bool retMouse = true;
            if (m_hMouseHook != 0)
            {
                retMouse = UnhookWindowsHookEx(m_hMouseHook);
                m_hMouseHook = 0;

            }

            //如果卸下钩子失败
            if (!(retMouse))
            {

            }
        }
        private int MouseHookProc(int in_nCode, Int32 in_wParam, IntPtr in_lParam)
        {
            //如果正常运行并且用户要监听鼠标的消息
            if ((in_nCode >= 0) && (MouseEvent != null))
            {
                MouseButton button = MouseButton.Middle;
                int clickCount = 0;
                ENxButtonActionTypes actionType = ENxButtonActionTypes.None;
                switch (in_wParam)
                {
                    //case WM_MOUSEMOVE:
                    //    {
                    //        //actionType = ButtonActionTypes.MouseMove;
                    //        break;
                    //    }
                    case WM_LBUTTONDOWN:
                        button = MouseButton.Left;
                        actionType = ENxButtonActionTypes.MouseDown;
                        clickCount = 1;
                        break;
                    case WM_LBUTTONUP:
                        button = MouseButton.Left;
                        actionType = ENxButtonActionTypes.MouseUp;
                        clickCount = 1;
                        break;
                    case WM_LBUTTONDBLCLK:
                        button = MouseButton.Left;
                        clickCount = 2;
                        break;
                    case WM_RBUTTONDOWN:
                        button = MouseButton.Right;
                        clickCount = 1;
                        break;
                    case WM_RBUTTONUP:
                        button = MouseButton.Right;
                        clickCount = 1;
                        break;
                    case WM_RBUTTONDBLCLK:
                        button = MouseButton.Right;
                        clickCount = 2;
                        break;
                }
                //从回调函数中得到鼠标的信息
                CNxMouseHookStruct MyMouseHookStruct = (CNxMouseHookStruct)Marshal.PtrToStructure(in_lParam, typeof(CNxMouseHookStruct));
                m_MouseArgs.Initialize(actionType, new Point(MyMouseHookStruct.pt.x, MyMouseHookStruct.pt.y), clickCount, button);
                MouseEvent(this, m_MouseArgs);
            }
            return CallNextHookEx(m_hMouseHook, in_nCode, in_wParam, in_lParam);
        }
    }
}
